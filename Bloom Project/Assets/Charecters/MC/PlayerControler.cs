using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public static PlayerControler current;
    //public float speed = 1f;
    public Animator anim;
    public Rigidbody2D rb;
    public Transform heightMarker;
    public Camera mainCam;
        
    public DialogController currentDialog;
    public npcController nearestNPC;
    public GameObject inventory;
    [SerializeField] bool inDialog;
    public bool overriden;
    public int lastDirection = 2;
    Transform _transform;
    SpriteRenderer _playerRenderer;

    public void Awake()
    {
        #region Failsafes
        if (_transform == null)
            _transform = transform;
        if (mainCam == null)
            mainCam = Camera.main;
        if (anim == null)
        {
            if (!TryGetComponent<Animator>(out anim))
            {
                Debug.LogError("No Animator On Player");
                enabled = false;
                return;
            }            
        }
        if(rb == null)
        {
            if(!TryGetComponent<Rigidbody2D>(out rb))
            {
                Debug.LogError("No Animator On Player");
                enabled = false;
                return;
            }            
        }
        if (_playerRenderer == null)
            _playerRenderer = GetComponent<SpriteRenderer>();
        if(current != this)
        {
            if(current != null)
            {
                current.enabled = false;
            }
            current = this;
        }
        #endregion
        lastDirection = 2;
    }
    private void Update()
    {
        if(!overriden)
        {
            _playerRenderer.sortingOrder = Mathf.CeilToInt(-_transform.position.y * 100f);
            SetLastDirection();
            Move();
            DialogControl();
        }
        else
        {
            SetToIdle();
        }
    }

    public void ToggleInDialog(bool _state)
    {
        inDialog = _state;
    }
    public bool InDialog() { return inDialog; }

    public void GrowPlant()
    {
         SpecialActionControler.current.isWatering = false;
    }
    private void SetLastDirection()
    {
        var w = Input.GetAxis("Vertical") > 0;
        var a = Input.GetAxis("Horizontal") < 0;
        var s = Input.GetAxis("Vertical") < 0;
        var d = Input.GetAxis("Horizontal") > 0;

        if (w)
        {
            lastDirection = 0;
        }
        else if (s)
        {
            lastDirection = 2;
        }
        
        if (a)
        {
            lastDirection = 1;
        }
        else if (d)
        {
            lastDirection = 3;
        }
    }

    private void DialogControl()
    {
        var inputTrigger = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0);
        if (currentDialog == null && nearestNPC != null)
        {
            if (inputTrigger)
            {                                
                nearestNPC.dialogController.TriggerDialog();                
            }
        }
        else if (currentDialog != null)
        {
            if (inputTrigger)
            {
                if (currentDialog.currentOption is DialogOption)
                {
                    if ((currentDialog.currentOption as DialogOption).branchText.Length != 0)
                    {
                        return;
                    }
                    else
                    {
                        currentDialog.Advance(0);
                    }
                }
                else 
                {
                    currentDialog.Advance(0);
                }
            }
        }
    }    

    public void TriggerAdvanceDialog(int _selection)
    {        
        if (currentDialog != null)
            currentDialog.Advance(_selection);
    }

    private void Move()
    {
        if (!inDialog && !AreaSwitcher.current.inTransition)
        {               
            Vector2 _input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if(_input.magnitude != 0)
            {
                if (rb.bodyType != RigidbodyType2D.Dynamic)
                    rb.bodyType = RigidbodyType2D.Dynamic;
                anim.SetFloat(MCAnimationStates.movement_x, _input.x);
                anim.SetFloat(MCAnimationStates.movement_y, _input.y);
            }
            else
            {
                SetToIdle();
            }

            var currentPos = new Vector2(_transform.position.x, _transform.position.y);
            if(_input.magnitude > 1f)
            {
                _input = _input.normalized * Mathf.Tan(Mathf.Deg2Rad * 45f);
            }
            var translation = currentPos + (_input * Time.deltaTime * SpecialActionControler.current.speed);            
            rb.MovePosition(translation);
        }
        else if(AreaSwitcher.current.inTransition)
        {
            if (Input.GetKey(KeyCode.W))
                lastDirection = 0;
            else
                lastDirection = 2;
            SetToIdle();
        }
        else
        {            
            if(rb.bodyType != RigidbodyType2D.Static)
            {
                rb.bodyType = RigidbodyType2D.Static;
                SetToIdle();
            }
        }
    }

    private void SetToIdle()
    {

        rb.velocity = Vector3.zero;
        anim.SetFloat(MCAnimationStates.movement_x, 0);
        anim.SetFloat(MCAnimationStates.movement_y, 0);
        anim.SetFloat(MCAnimationStates.idle, lastDirection);
    }

    public float GetY()
    {
        return heightMarker.position.y;
    }        
}

public static class MCAnimationStates
{
    public static string movement_x = "movement_x";
    public static string movement_y = "movement_y";
    public static string idle = "idle";
}
