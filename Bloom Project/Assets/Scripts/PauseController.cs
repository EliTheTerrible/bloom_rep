    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour
{
    public static PauseController current;
    public GameObject pauseRootObject;
    public GameObject pauseDefaultScreen;
    public GameObject[] pauseScreens;
    public Slider master, sfx, music, d_speed;
    public VolumeManager vm;
    public bool pauseTime;
    bool isPaused = false;
    PlayerControler player;


    public void OnEnable()
    {
        StartCoroutine(Startup());
    }

    private IEnumerator Startup()
    {
        if (current != this)
        {
            if (current != null)
                Destroy(current.gameObject);
        }
        current = this;
        master.value = VolumeManager.master;
        sfx.value = VolumeManager.sfxCurrent;
        music.value = VolumeManager.musicCurrent;
        yield return new WaitUntil(() => PlayerControler.current != null);
        player = PlayerControler.current;
        SetPause(false);
    }

    private void Update()
    {
        if (player == null) return;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!player.InDialog())
            {
                TogglePause();
            }
            else
            {
                if (player.currentDialog.IsTyping())
                    player.currentDialog.SkipToEndOfTypeOut();
                else
                    player.currentDialog.EndDialog();
            }
        }
    }

    private void SetPause(bool _pause)
    {
        isPaused = _pause;
        if (pauseTime)
            Time.timeScale = _pause ? 0f : 1f;
        player.enabled = !_pause;
        pauseRootObject.SetActive(_pause);
        if(_pause)
        {
            ShowDefaultScreen();
        }
    }

    private void ShowDefaultScreen()
    {
        for (int i = 0; i < pauseScreens.Length; ++i)
        {
            if (pauseScreens[i] != pauseDefaultScreen)
            {
                pauseScreens[i].SetActive(false);
            }
            else
            {
                pauseDefaultScreen.SetActive(true);
            }
        }
    }

    public void TogglePause()
    {
        SetPause(!isPaused);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
