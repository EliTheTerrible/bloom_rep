﻿using UnityEngine;
using System.Collections;

public class PlantControler : MonoBehaviour
{    
    public DialogController plantDialog;
    public DialogOption doneGrowing;
    public SpriteRenderer renderer;
    public Sprite seed, mid, done;
    public Item growenCrystal;
        
    public void Plant()
    {
        renderer.sprite = seed;
    }

    public void Water()
    {
        WaterManager.current.UseCan();
        if (renderer.sprite == seed)
        {
            renderer.sprite = mid;
        }
        else if (renderer.sprite == mid)
        {
            renderer.sprite = done;
            plantDialog.defaultOption = (doneGrowing as Dialog);
            plantDialog.currentOption = null;              
        }
    }    

    public void Harvest()
    {
        renderer.sprite = null;
        InventoryManager.current.AddItem(growenCrystal);
    }
}
