﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AreaSwitcher : MonoBehaviour
{
    public bool enableAutotransmission;
    public static AreaSwitcher current;
    public bool transitioning;

    public float transitionHalfTime;
    public Area[] areas;    
    public Image blackout;
    public bool canTransition = false;
    public bool inTransition;
    public Area currentArea;
    Area nextArea;
    
    public void OnEnable()
    {
        if(current != this)
        {
            if(current != null)
            {
                Destroy(gameObject);
            }
            current = this;
        }
        StartCoroutine(FadeToScene());
        foreach(Area _currentArea in areas)
        {
            if(_currentArea.gameObject.activeSelf == true)
            {
                currentArea = _currentArea;
            }
        }
    }

    public void Update()
    {
        var trigger = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0);
        if(!inTransition && canTransition && trigger)
        {
            SwitchArea();
        }
    }


    public void SwitchArea()
    {
        StartCoroutine(Transiotion());
    }

    IEnumerator Transiotion()
    {
        inTransition = true;
        yield return new WaitForEndOfFrame();
        currentArea.entranceSound.Play();
        yield return FadeToBlack();
        ChangeSets();

        yield return FadeToScene();
        inTransition = false;
    }

    private void ChangeSets()
    {
        var _nextArea = nextArea;
        currentArea.gameObject.SetActive(false);
        currentArea = _nextArea;
        currentArea.gameObject.SetActive(true);
    }

    public IEnumerator FadeToBlack()
    {
        var timer = 0f;
        while (timer <= transitionHalfTime)
        {
            timer += Time.unscaledDeltaTime;
            var alphaState = Mathf.Clamp01(timer / transitionHalfTime);
            blackout.color = new Vector4(0f, 0f, 0f, alphaState);
            yield return null;
        }
    }
    public IEnumerator FadeToScene()
    {
        var timer = transitionHalfTime;
        while (timer >= 0f)
        {
            timer -= Time.unscaledDeltaTime;
            var alphaState = Mathf.Clamp01(timer / transitionHalfTime);
            blackout.color = new Vector4(0f, 0f, 0f, alphaState);
            yield return null;
        }

    }

    public void EnableTransition(Area _areaToTransiotion)
    {
        canTransition = true;
        nextArea = _areaToTransiotion;        
    }
    public void AutoTransition(Area _areaToTransiotion)
    {
        if(!inTransition)
        {
            canTransition = true;
            nextArea = _areaToTransiotion;
            if (enableAutotransmission)
            {
                SwitchArea();
            }
        }
    }

    public void DisableTransition()
    {
        canTransition = false;
        nextArea = null;
    }    
}
