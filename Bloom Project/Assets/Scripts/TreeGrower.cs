﻿using UnityEngine;
using System.Collections;

public class TreeGrower : MonoBehaviour
{

    public SpriteRenderer sick, growen;
    
    public void Initialize()
    {
        sick.color = Color.white;
        growen.color = new Vector4(1f, 1f, 1f, 0f);
    }

    public void StartGrowth(float time)
    {
        StartCoroutine(WaitForPlayer(time));
    }

    IEnumerator WaitForPlayer(float _time)
    {
        yield return new WaitUntil(() => PlayerControler.current.transform.position.x >= transform.position.x);
        var timer = 0f;
        while(timer <= _time)
        {
            var _phase = timer / _time;
            growen.color = new Vector4(1f, 1f, 1f, _phase);
            timer += Time.deltaTime;
            yield return null;
        }
        sick.color = new Vector4(1f, 1f, 1f, 0f);
        growen.color = Color.white;        
    }
}
