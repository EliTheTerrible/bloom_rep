﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(npcController))]
public class DialogController : MonoBehaviour
{
    public Dialog standbyOption;
    public Dialog currentOption;    
    public Dialog defaultOption;
    public DialogBoxRelay dialog_pic, dialog_noPic;
    public GameObject inRangeMarker;

    DialogBoxRelay lastUsedDialogBox;
    public float typeOutSpeed;
    bool typing, checkingItem;
    public npcController npc;    


    private void OnEnable()
    {
        if (npc == null)
            npc = GetComponent<npcController>();
        npc.dialogController = this;
        dialog_noPic.gameObject.SetActive(false);
        dialog_pic.gameObject.SetActive(false);
    }
            
    public bool IsTyping()
    {
        return typing;
    }
    public void SetIndicator(bool _inRange)
    {
        if (inRangeMarker == null)
            return;
        if (defaultOption != null)
            inRangeMarker.SetActive(_inRange);
        else
            inRangeMarker.SetActive(false);
        
    }      

    public void TriggerDialog()
    {
        #region Failsafes
        if (defaultOption == null)
            return;                
        if(currentOption == null)
        {
            currentOption = defaultOption;
        }
        #endregion       
        if(!PlayerControler.current.InDialog())
        {
            PlayerInDialog();
            if (currentOption is DialogOptionItem)
            {
                StartCoroutine(UpdateDialog((currentOption as DialogOptionItem).GetOption()));
            }
            else if (currentOption is DialogOption)
                StartCoroutine(UpdateDialog(currentOption as DialogOption));
        }

    }

    private void PlayerInDialog()
    {
        PlayerControler.current.currentDialog = this;
        PlayerControler.current.ToggleInDialog(true);
        PlayerControler.current.inventory.SetActive(false);
    }
    private void PlayerOutOfDialog()
    {
        PlayerControler.current.ToggleInDialog( false);
        PlayerControler.current.currentDialog = null;
        PlayerControler.current.inventory.SetActive(true);
        dialog_pic.gameObject.SetActive(false);
        dialog_noPic.gameObject.SetActive(false);
    }

    public void EndDialog()
    {        
        if(PlayerControler.current != null)
        {
            if (PlayerControler.current.currentDialog == this)
            {
                PlayerOutOfDialog();
            }
        }
    }


    public void Advance(int _branch)
    {        
        if(!PlayerControler.current.InDialog())
        {
            PlayerInDialog();
        }        

        if (currentOption == null)
        {
            currentOption = defaultOption;
            EndDialog();
            return;
        }
        if (!typing)
        {
            SelectBranch(_branch);
        }
        else
        {
            typing = false;
        }
    }
    public void SelectBranch(int _selection)
    {                
        if(currentOption is DialogOption)
        {            
            if ((currentOption as DialogOption).nextOption.Count > 0)
            {
                var nextDialog = (currentOption as DialogOption).nextOption[_selection];           
                currentOption = nextDialog;
                StartCoroutine(UpdateDialog(nextDialog as DialogOption));
            }
            else
            {
                EndDialog();
            }                        
        }
        else if(currentOption is DialogOptionItem)
        {
            DialogOption nextDialog;
            if(!checkingItem)
            {
                nextDialog = (currentOption as DialogOptionItem).GetOption();
                checkingItem = true;
            }
            else
            {
                checkingItem = false;
                if((currentOption as DialogOptionItem).GetOption().nextOption.Count > 0)
                {     
                    nextDialog = (((currentOption as DialogOptionItem).GetOption() as DialogOption).nextOption[_selection] as DialogOption);
                    currentOption = nextDialog;
                    StartCoroutine(UpdateDialog(nextDialog));
                }
                else
                {
                    EndDialog();
                }            
            }
        }
    }
    public void SkipToEndOfTypeOut()
    {
        typing = false;
    }
 

    private IEnumerator UpdateDialog(DialogOption branchToShow)
    {
        typing = false;                
        StartOptionActions(branchToShow);

        if (branchToShow.text != "" || branchToShow.branchText.Length > 0)
        {
            var relayToUse = SelectRelay(branchToShow);
            SetSprite(branchToShow, relayToUse);        
            relayToUse.gameObject.SetActive(true);
            yield return SetButtons(branchToShow, relayToUse);            
            yield return Typeout(branchToShow.text, relayToUse, false);
            yield return new WaitForEndOfFrame();
        }

        // empty dialog will skip to next option avilable or will loop if none added
        else if ((branchToShow.text == "") && (branchToShow.branchText.Length== 0)) 
        {
            EndDialog();                        
            if(branchToShow.nextOption.Count > 0)
            {
                currentOption = branchToShow.nextOption[0];
            }
        }
        yield return new WaitForEndOfFrame();
        DemoQuest.current.AddSeenDialog(branchToShow);
    }
    private IEnumerator SetButtons(DialogOption optionToShow, DialogBoxRelay _relayToUse)
    {        
        if(optionToShow.branchText.Length > 0)
        {            
            yield return new WaitForEndOfFrame();
            foreach(TMP_Text _textToReset in _relayToUse.selectionText)
            {
                _textToReset.text = "";
                _textToReset.gameObject.SetActive(true);
            }            
            foreach(Button _buttonToDisable in _relayToUse.buttons)
            {
                _buttonToDisable.interactable = false;
            }
            
            for(int i = 0; i < optionToShow.branchText.Length; ++i)
            {
                _relayToUse.selectionText[i].text = optionToShow.branchText[i];
                _relayToUse.selectionText[i].maxVisibleCharacters = 0;
            }
            
            for (int i = 0; i < optionToShow.branchText.Length ; ++i)
            {
                _relayToUse.dialogToUse = i;
                yield return StartCoroutine(Typeout(optionToShow.branchText[i], _relayToUse,true));
            }            
            for(int i = 0; i < optionToShow.branchText.Length; ++i)
            {
                _relayToUse.buttons[i].interactable = true;
            }
        }
        else
        {            
            foreach (TMP_Text _textToReset in _relayToUse.selectionText)
            {
                _textToReset.text = "";
                _textToReset.gameObject.SetActive(false);
            }
            foreach (Button _buttonToDisable in _relayToUse.buttons)
            {
                _buttonToDisable.interactable = false;
            }
        }
    }    
    private IEnumerator Typeout(string textToType, DialogBoxRelay _relay, bool _isSelection)
    {        
        var textComponentToUse = _isSelection ? _relay.selectionText[_relay.dialogToUse] : _relay.text;        
        if(textToType != "")
        {            
            textComponentToUse.text = textToType;
            if(typeOutSpeed == 0)
            {
                textComponentToUse.maxVisibleCharacters = textToType.Length;
                yield break;
            }

            typing = true;
            for (int typeOutStep = 0; typeOutStep <= textToType.Length; ++typeOutStep)
            {                
                textComponentToUse.maxVisibleCharacters = typeOutStep;
                yield return new WaitForSeconds(1 / typeOutSpeed);                
                if(!typing)
                {
                    textComponentToUse.maxVisibleCharacters = textToType.Length;                    
                    typeOutStep = textToType.Length + 1;
                    yield return new WaitForEndOfFrame();                    
                }
            }
            typing = false;
        }
        else
        {
            textComponentToUse.text = "";
        }
    }


    private static void StartOptionActions(DialogOption optionToShow)
    {
        if (optionToShow.actions.Length != 0)
        {
            foreach (UnityEvent _actionToStart in optionToShow.actions)
            {
                _actionToStart.Invoke();
            }
        }
    }
    private static void SetSprite(DialogOption optionToShow, DialogBoxRelay _dialogToUse)
    {
        if (_dialogToUse.avatar != null)
        {
            _dialogToUse.avatar.sprite = optionToShow.expresion;
        }
    }
    private DialogBoxRelay SelectRelay(DialogOption optionToShow)
    {
        var _relayToUse = (optionToShow.expresion == null) ? dialog_noPic : dialog_pic;
        if (lastUsedDialogBox != _relayToUse)
        {
            if (lastUsedDialogBox != null)
            {
                lastUsedDialogBox.gameObject.SetActive(false);
            }
            lastUsedDialogBox = _relayToUse;
        }    
        if(_relayToUse.text != null)
        {
            _relayToUse.text.text = "";
        }
        if(_relayToUse.selectionText.Length != 0)
        {
            if(_relayToUse.buttons.Count == 0)
            {
                _relayToUse.buttons = new List<Button>();
                for(int i = 0; i < _relayToUse.selectionText.Length; ++i)
                {
                     _relayToUse.buttons.Add(_relayToUse.selectionText[i].GetComponent<Button>());
                }
            }
            foreach(TMP_Text _textToReset in _relayToUse.selectionText)
            {
                _textToReset.text = "";
            }
        }
        return _relayToUse;
    }


}