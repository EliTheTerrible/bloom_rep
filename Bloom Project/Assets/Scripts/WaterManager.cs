﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaterManager : MonoBehaviour
{
    public static WaterManager current;
    public int waterRemainig;
    public int portions;
    public Item empty, notEmpty;

    private void Awake()
    {
        if(current != this)
        {
            if (current != null)
            {
                current.enabled = false;
            }
            current = this;
        }
    }

    private void Update()
    {
        if(waterRemainig == 0 && InventoryManager.current.selectedItem == notEmpty)
        {
            InventoryManager.current.selectedItem = empty;
        }
        else if(waterRemainig > 0 && InventoryManager.current.selectedItem == empty)
        {
            InventoryManager.current.selectedItem = notEmpty;
        }
    }

    public void Refill()
    {
        waterRemainig = portions;
        Image _watercanSlot = null;
        foreach(Image _slot in InventoryManager.current.inventoryState.Keys)
        {
            if(InventoryManager.current.inventoryState[_slot] == empty)
            {
                _watercanSlot = _slot;
                if(InventoryManager.current.selectedItem == empty)
                {
                    InventoryManager.current.selectedItem = notEmpty;
                }
            }
        }
        if (_watercanSlot == null)
        {
            Debug.Log("No slot found");
            return;
        }
        InventoryManager.current.inventoryState[_watercanSlot] = notEmpty;
        InventoryManager.current.UpdateSprites();
    }

    public bool HasWater()
    {
        if (waterRemainig == 0)
        {
            return false;
        }
        else
            return true;
    }

    public void UseCan()
    {        
        waterRemainig = waterRemainig -1;
        if(waterRemainig == 0)
        {
            Image _watercanSlot = null;
            foreach (Image _slot in InventoryManager.current.inventoryState.Keys)
            {
                if (InventoryManager.current.inventoryState[_slot] == notEmpty)
                {
                    _watercanSlot = _slot;
                    if (InventoryManager.current.selectedItem == notEmpty)
                    {
                        InventoryManager.current.selectedItem = empty;
                    }
                }
            }
            if (_watercanSlot == null)
            {
                Debug.Log("No slot found");
                return;
            }
            InventoryManager.current.inventoryState[_watercanSlot] = empty;
            InventoryManager.current.UpdateSprites();
        }
    }
}
