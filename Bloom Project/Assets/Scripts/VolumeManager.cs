﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

[CreateAssetMenu(menuName = ("Volume Manager"))]
public class VolumeManager : ScriptableObject
{
    public AudioMixer sfx, music;
    public static float musicCurrent = 0.5f;
    public static float sfxCurrent = 0.5f;
    public static float master = 0.5f;


    public void SetVolumeSFX(float volume)
    {
        sfxCurrent = volume;
        if(volume == 0 || master == 0)
        {
            var mute = -80f;
            sfx.SetFloat("Volume", mute);
            return;
        }
        var level = Mathf.Log10(volume * master) * 20f;
        sfx.SetFloat("Volume", level);
    }

    public void SetVolumeMusic(float volume)
    {
        musicCurrent = volume;
        if(volume == 0 || master == 0)
        {
            var mute = -80;
            music.SetFloat("Volume", mute);
            return;
        }
        var level = Mathf.Log10(volume * master) * 20f;
        
        music.SetFloat("Volume", level);
    }

    public void SetMaster(float _master)
    {
        master = _master;
        UpdateMasterVolume();
    }

    public void UpdateMasterVolume()
    {
        SetVolumeSFX(sfxCurrent);
        SetVolumeMusic(musicCurrent);
    }

    
}
