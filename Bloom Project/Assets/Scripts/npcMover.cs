﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class npcMover : MonoBehaviour
{
    public List<Transform> roamWP = new List<Transform>();
    npcController controller;
    Vector3 currentTarget;
    Transform _transform;
    Rigidbody2D rb;
    Animator charAnim;    
    float stuckTimer;
    float nextMoveTime;
    public float minWaitTime, maxWaitTime, defaultSpeed;
    public bool freeze;
    public void OnEnable()
    {
        _transform = transform;
        if (controller == null)
            controller = GetComponent<npcController>();
        if (rb == null)
            rb = GetComponent<Rigidbody2D>();
        if (charAnim == null)
            charAnim = GetComponent<Animator>();       
        else
            currentTarget = transform.position;
    }

    private void Update()
    {
        if(controller.roam || controller.Overriden)
        {
            if (Vector3.Distance(_transform.position, currentTarget) < 0.1)
            {        
                charAnim.SetFloat("Mov_Y", 0f);
                charAnim.SetFloat("Mov_X", 0f);
                if(!controller.Overriden && Time.time > nextMoveTime && !freeze)
                {                    
                    SetWP();
                }
                else
                {
                    charAnim.SetFloat("Mov_Y", 0f);
                    charAnim.SetFloat("Mov_X", 0f);
                }
                return;
            }

            var dirToTarget = (currentTarget - _transform.position).normalized;
            if(!freeze)
            {
                rb.MovePosition(_transform.position +( dirToTarget * Time.deltaTime * controller.roamSpeed));
                charAnim.SetFloat("Mov_Y", dirToTarget.y);
                charAnim.SetFloat("Mov_X", dirToTarget.x);
            }
            else
            {
                charAnim.SetFloat("Mov_X", 0f);
                charAnim.SetFloat("Mov_Y", 0f);
            }
        }     
        else if(!controller.roam)
        {
            charAnim.SetFloat("Mov_X", 0f);
            charAnim.SetFloat("Mov_Y", 0f);
        }
    }

    private void SetWP()
    {        
        currentTarget = roamWP[Random.Range(0,roamWP.Count)].position;
        var dT = (Random.Range(minWaitTime, maxWaitTime) + Random.Range(minWaitTime, maxWaitTime) + Random.Range(minWaitTime, maxWaitTime)) / 3f;
        nextMoveTime = Time.time + dT;
        freeze = false;
    }

    

    public void Teleport(Vector3 _pos, bool _hold)
    {
        controller.transform.position = _pos;
        controller.roamCenter.transform.position = _pos;
        currentTarget = _pos;
        freeze = _hold;
        controller.roam = !_hold;
    }
    
    public IEnumerator WalkPath(List<Transform> _path)
    {        
        controller.roamSpeed = defaultSpeed;
        freeze = false;
        for(int i = 0; i < _path.Count; ++i)
        {
            currentTarget = _path[i].position;
            if(_path[i].tag != "Player")
            {
                yield return new WaitUntil(() => Vector3.Distance(_transform.position, _path[i].position) <= 0.1f);
            }
            else
            {
                yield return new WaitUntil(() => Vector3.Distance(_transform.position, _path[i].position) <= 1f);
            }
        }                
    }
}
