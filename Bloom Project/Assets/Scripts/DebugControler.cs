using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugControler : MonoBehaviour
{
    public GameObject debugWindow;

    private void OnEnable()
    {
        var state = 0;//PlayerPrefs.GetInt("ShowDebug", 0);

        if(state == 0)
        {
            debugWindow.SetActive(false);
        }
        else
        {
            debugWindow.SetActive(true);
        }

        //StartCoroutine(Toggle());
    }

    public IEnumerator Toggle()
    {
        while(enabled)
        {
            yield return new WaitUntil(()=> (Input.GetKey(KeyCode.Z) && Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.X)));
            var state = PlayerPrefs.GetInt("ShowDebug", 0);            

            if (state == 0)
            {
                PlayerPrefs.SetInt("ShowDebug", 1);
                debugWindow.SetActive(true);
            }
            else
            {
                PlayerPrefs.SetInt("ShowDebug", 0);
                debugWindow.SetActive(false);
            }
            yield return new WaitUntil(() => (!Input.GetKey(KeyCode.Z) || !Input.GetKey(KeyCode.C) || !Input.GetKey(KeyCode.X)));
        }
    }
}
