using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepController : MonoBehaviour
{
    public AudioSource step_1, step_2, effect;
    AudioSource asInUse;
    public int creak_Chance;
    public List<AudioClip> outside_steps = new List<AudioClip>();
    public List<AudioClip> inSide_steps = new List<AudioClip>();

    public void Step()
    {
        if (asInUse == null)
        {
            asInUse = step_1;
        }
        else if (asInUse == step_1)
            asInUse = step_2;
        else if (asInUse == step_2)
            asInUse = step_1;
        if (AreaSwitcher.current.currentArea.tag == "Extirrior")
        {
            var _step = outside_steps[Random.Range(0, outside_steps.Count)];
            asInUse.Stop();
            asInUse.PlayOneShot(_step);
        }
        else if(AreaSwitcher.current.currentArea.tag == "Interrior")
        {
            var _step = inSide_steps[Random.Range(0, inSide_steps.Count)];
            asInUse.Stop();
            asInUse.PlayOneShot(_step);
            var _chance = Random.Range(0, creak_Chance);
            if (_chance == 0)
                effect.PlayOneShot(effect.clip);
        }
    }
}
