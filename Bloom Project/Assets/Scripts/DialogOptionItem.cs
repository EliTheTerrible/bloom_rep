﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[CreateAssetMenu(menuName = "Item Locked Dialog Option")]
public class DialogOptionItem : Dialog
{
    public List<ItemDailog> itemDialogs = new List<ItemDailog>();
    public DialogOption noItem;
    public DialogOption itemNotSelected;

    public DialogOption GetOption()
    {
        if(InventoryManager.current.selectedItem == null)
        {
            return noItem;
        }
        foreach (ItemDailog _itemToCheck in itemDialogs)
        {
            if(InventoryManager.current.selectedItem.itemName == _itemToCheck.requiredItemName)
            {
                return _itemToCheck.itemSeleced;
            }
        }
        return itemNotSelected;
    }

    [System.Serializable]
    public struct ItemDailog
    {
        public string requiredItemName;
        public DialogOption itemSeleced;        
    }
}
