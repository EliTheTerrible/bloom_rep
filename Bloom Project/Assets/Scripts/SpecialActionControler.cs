using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SpecialActionControler : MonoBehaviour
{
    public static SpecialActionControler current;
    public float speed = 4f;
    [Header("Paths")]
    public List<Transform> Pos_1toSFhouse = new List<Transform>();
    public List<Transform> SFhouseToPlayer = new List<Transform>();
    public List<Transform> treeToBridge = new List<Transform>();
    [Header("Positional Refrences")]
    public Transform sf_home;
    public Transform at_pos;
    [Header("NPCs")]
    public npcController sunflower;
    public npcController watercan_object, playerMailbox, ancientTree, bridge;
    [Header("Items")]
    public Item watercan_item;
    public Item earth_gem_seed, air_gem_seed, water_gem_seed, fire_gem_seed;
    public Item earth_gem_growen, air_gem_growen, water_gem_growen, fire_gem_growen;
    public Item wood;
    [Header("Saucers")]
    public PlantControler air;
    public PlantControler fire, earth, water;
    [Header("SFXs")]
    public AudioClip door_close;
    public AudioClip door_open;
    [Header("Misc")]
    public float treesGrowthTime;
    public float seedRetrievalTime;
    public TreeGrower[] trees;
    public WaterManager watercanController;
    public AudioSource playerSfx;
    [Header("UI")]
    public TMP_Text debug_playerSpeed;
    Dictionary<Item, PlantControler> saucers = new Dictionary<Item, PlantControler>();
    [HideInInspector] public bool isWatering;


    public void Awake()
    {
        if (current != this)
        {
            if (current != null)
            {
                current.enabled = false;
            }
            current = this;
        }
        if (saucers.Count == 0)
        {
            saucers.Add(air_gem_seed, air);
            saucers.Add(earth_gem_seed, earth);
            saucers.Add(fire_gem_seed, fire);
            saucers.Add(water_gem_seed, water);
        }
        if(trees.Length == 0)
        {
            trees = FindObjectsOfType<TreeGrower>();
            foreach(TreeGrower _tree in trees)
            {
                _tree.Initialize();
            }
        }
        speed = 4f;//StartCoroutine(SetSpeed());
    }
    IEnumerator SetSpeed()
    {
        yield return new WaitUntil(() => (PlayerControler.current != null) && (PauseController.current != null));
        speed = PlayerPrefs.GetFloat("WalkSpeed");
        if (speed == 0f)
        {
            speed = 4f;
            PlayerPrefs.SetFloat("WalkSpeed", speed);
        }
        PauseController.current.d_speed.value = speed;
    }
    public void UpdatePlayerSpeed(float value)
    {
        if(debug_playerSpeed!= null)
        {
            speed = value;
            PlayerPrefs.SetFloat("WalkSpeed", value - (value % 0.01f));
            debug_playerSpeed.text = (value - (value % 0.01f)).ToString();
        }
    }


    public void TriggerTreesHealing()
    {
        foreach(TreeGrower _tree in trees)
        {
            _tree.StartGrowth(treesGrowthTime);
        }
    }
    public void AddItem(Item _itemToAdd)
    {
        InventoryManager.current.AddItem(_itemToAdd);
    }

    public void TeleportNPCToPos(npcController _npc, Vector3 _pos, bool _roam)
    {
        _npc.mover.Teleport(_pos, _roam);
    }    

    public void StartSF_GetSeed()
    {
        StartCoroutine(SunflowerRetrivesSeeds());
    }

    IEnumerator SunflowerRetrivesSeeds()
    {
        PlayerControler.current.overriden = true;
        sunflower.Overriden = true;
        for(int i = 0; i < sunflower.mainColliders.Length; ++i)
        {
            sunflower.mainColliders[i].enabled = false;
        }
        sunflower.dialogController.EndDialog();
        yield return StartCoroutine(sunflower.mover.WalkPath(Pos_1toSFhouse));
        SpecialActions.PlaySFX(door_open);
        sunflower.gameObject.SetActive(false);
        yield return new WaitForSeconds(door_open.length + seedRetrievalTime);        
        SpecialActions.PlaySFX(door_close);
        yield return new WaitForSeconds(door_close.length/2);
        sunflower.gameObject.SetActive(true);
        yield return StartCoroutine(sunflower.mover.WalkPath(SFhouseToPlayer));
        PlayerControler.current.overriden = false;
        sunflower.dialogController.Advance(0);
        sunflower.Overriden = false;
        for (int i = 0; i < sunflower.mainColliders.Length; ++i)
        {
            sunflower.mainColliders[i].enabled = true;
        }
    }


    public void Plant(Item _seedToPlant)
    {
        _seedToPlant.transform.parent = saucers[_seedToPlant].transform;
        saucers[_seedToPlant].Plant();
        InventoryManager.current.RemoveSelectedItem();
    }

    public void StartWatering(Item _crystalToWater)
    {
        if (watercanController.HasWater())
        {
            StartCoroutine(WateringProcess(_crystalToWater));
        }
    }    

    IEnumerator WateringProcess(Item _crystalToWater)
    {
        isWatering = true;
        PlayerControler.current.overriden = true;
        var trigger = ((saucers[_crystalToWater].transform.position.x - PlayerControler.current.transform.position.x) > 0f) ? "water_l" : "water_r";
        PlayerControler.current.anim.SetTrigger(trigger);
        yield return new WaitUntil(() => !isWatering);
        saucers[_crystalToWater].Water();
        PlayerControler.current.overriden = false;
    }

    public void Harvest(Item _crystal)
    {
        saucers[_crystal].Harvest();
    }

    public void WalkToBridge()
    {
        StartCoroutine(endgame());
    }

    IEnumerator endgame()
    {
        for (int i = 0; i < sunflower.mainColliders.Length; ++i)
        {
            sunflower.mainColliders[i].enabled = false;
        }
        sunflower.Overriden = true;
        yield return sunflower.mover.WalkPath(treeToBridge);
        sunflower.Overriden = true;
        yield return new WaitForEndOfFrame();
        sunflower.mover.enabled = false;
        yield return new WaitUntil(() => bridge.dialogTrigger.InRange());
        yield return StartCoroutine(AreaSwitcher.current.FadeToBlack());
        UnityEngine.SceneManagement.SceneManager.LoadScene("Outro Cutscene");
    }
}
