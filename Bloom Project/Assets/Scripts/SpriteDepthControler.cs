using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteDepthControler : MonoBehaviour
{    
    Transform _transform;    
    public List<SubSprite> childSprites = new List<SubSprite>();
    public Dictionary<SubSprite, int> defaultOrder = new Dictionary<SubSprite, int>();
    private void OnEnable()
    {
        
        if (_transform == null)
            _transform = transform;        
        if (childSprites.Count == 0)
        {
            var parentName = _transform.parent == null ? "world" : _transform.parent.name;
            Debug.LogError("No Sprite Renderers added to " + gameObject.name + " in " + parentName);
            return;
        }
        foreach(SubSprite _child in childSprites)
        {
            if(!defaultOrder.ContainsKey(_child))
                defaultOrder.Add(_child, _child.sprite.sortingOrder);
        }
    }

    public void Update()
    {
        if (PlayerControler.current == null)
            return;        
        if(PlayerControler.current == null)
        {
            return;
        }        
        foreach (SubSprite _subsprite in childSprites)
        {
            var _orderToSet = Mathf.CeilToInt(-_transform.position.y * 100f);
            _subsprite.sprite.sortingOrder = _orderToSet + _subsprite.orderOffset;
        }
    }
}

[System.Serializable]
public struct SubSprite
{
    public SpriteRenderer sprite;
    public int orderOffset;
}
