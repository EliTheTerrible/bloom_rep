﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DialogBoxRelay : MonoBehaviour
{
    public TMP_Text text;
    public TMP_Text[] selectionText;
    public Image avatar;
    [HideInInspector] public int dialogToUse;
    [HideInInspector] public List<Button> buttons;
}
