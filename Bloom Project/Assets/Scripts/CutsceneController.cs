using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CutsceneController : MonoBehaviour
{
    public enum Transition_In { whiteIn, blackIn, none}
    public enum Transition_Out { whiteOut, blackOut, x_fade, none}
    public KeyCode[] skipKey = { KeyCode.Space, KeyCode.Mouse0};
    public bool autoStart;
    public Image fade;
    public Image backdrop;
    public CutsceneElement[] cutsceneElements;
    public float typeOutSpeed = 10f;
    public string nextSceneName;   
    bool typing;
    bool cutsceneInProgress;    

    public void OnEnable()
    {
        if (autoStart)
            StartCoroutine(PlayCutscene());
    }

    bool CheckSkip()
    {        
        foreach(KeyCode keyToCheck in skipKey)
        {
            if (Input.GetKeyDown(keyToCheck))
                return true;
        }
        return false;
    }    
    
    IEnumerator PlayCutscene()
    {
        cutsceneInProgress = true;
        if (cutsceneElements.Length == 0)
            Debug.Log("No Cutscene Elements");        

        for (int cutsceneElementToShow = 0; cutsceneElementToShow < cutsceneElements.Length; ++cutsceneElementToShow)
        {
            var currentElement = cutsceneElements[cutsceneElementToShow];
            UpdateBackdrop(currentElement);

            #region Fade in
            if(currentElement.dialogWindow != null)
            {
                currentElement.dialogWindow.gameObject.SetActive(false);
            }
            if ((currentElement.t_In == Transition_In.whiteIn) || (currentElement.t_In == Transition_In.blackIn))
            {
                var color = currentElement.t_In == Transition_In.whiteIn ? 1f : 0f;
                yield return StartCoroutine(FadeIn(color, currentElement.transitoinTime));
                fade.color = new Vector4(fade.color.r, fade.color.g, fade.color.b, 0f);
            }
            else
            {
                fade.color = new Vector4(fade.color.r, fade.color.g, fade.color.b, 0f);
            }
            var _triggerTime = Time.time + currentElement.transiontDelay;
            
            yield return new WaitForEndOfFrame();

            #endregion
            #region Pre Dialog Action execution
            foreach (UnityEngine.Events.UnityEvent action in currentElement.preDialogAction)
            {
                action.Invoke();
            }
            #endregion
            #region Typeout
            if (currentElement.dialogWindow != null)
            {
                typing = true;
                currentElement.dialogWindow.gameObject.SetActive(true);
                yield return Typeout(currentElement);
                typing = false;
            }
            #endregion
            #region Close Dialog and Post Dialog Actions
            yield return new WaitUntil(() => !CheckSkip());
            yield return new WaitUntil(() => CheckSkip());            
            if (currentElement.dialogWindow != null)
            {
                currentElement.dialogWindow.gameObject.SetActive(false);
            }
            foreach (UnityEngine.Events.UnityEvent _postDialogAction in currentElement.postDialogAction)
            {
                _postDialogAction.Invoke();
            }            
            #endregion
            
            #region Fade Out
            yield return new WaitForEndOfFrame();
            _triggerTime = Time.time + currentElement.transiontDelay;
            yield return new WaitUntil(() => ((Time.time >= _triggerTime) || CheckSkip()));
            #region At Transition Action execution
            foreach(UnityEngine.Events.UnityEvent action in currentElement.atTransitionAction)
            {
                action.Invoke();
            }
            #endregion            
            yield return new WaitForEndOfFrame();
            if ((currentElement.t_Out == Transition_Out.whiteOut) || (currentElement.t_Out == Transition_Out.blackOut))
            {
                var color = currentElement.t_Out == Transition_Out.whiteOut ? 1f : 0f;
                yield return StartCoroutine(FadeOut(color, currentElement.transitoinTime));
            }
            else if(currentElement.t_Out == Transition_Out.x_fade)
            {
                if (cutsceneElementToShow < cutsceneElements.Length - 1)
                {
                    yield return StartCoroutine(CrossFade(cutsceneElements[cutsceneElementToShow + 1].backgroudImage, currentElement.transitoinTime));
                }
                else
                {
                    yield return StartCoroutine(FadeOut(1f, currentElement.transitoinTime));
                }
            }
            #endregion            
        }
        
        SceneManager.LoadSceneAsync(nextSceneName, LoadSceneMode.Single);
        yield return null;
    }

    private IEnumerator Typeout(CutsceneElement currentElement)
    {
        currentElement.dialogWindow.text.text = currentElement.text;
        currentElement.dialogWindow.text.maxVisibleCharacters = 0;
        for (int typeOutStep = 0; typeOutStep <= currentElement.text.Length; ++typeOutStep)
        {            
            currentElement.dialogWindow.text.maxVisibleCharacters = typeOutStep;
            var _nextCharTime = Time.time + 1 / typeOutSpeed;
            yield return new WaitUntil(() => (Time.time >= _nextCharTime)||CheckSkip());                        
            if (CheckSkip())
            {
                currentElement.dialogWindow.text.maxVisibleCharacters = currentElement.text.Length;
                typeOutStep = currentElement.text.Length +1;
                yield break;
            }
        }
    }
    private void UpdateBackdrop(CutsceneElement currentElement)
    {
        if (currentElement.backgroudImage != null)
        {
            backdrop.color = Color.white;
            backdrop.sprite = currentElement.backgroudImage;
        }
        else
        {
            backdrop.color = Color.red;
        }
    }
    #region Fades
    IEnumerator CrossFade(Sprite nextImage, float time)
    {
        yield return new WaitForEndOfFrame();
        var timer = 0f;
        fade.sprite = nextImage;
        fade.color = Vector4.zero;
        if (time == 0)
        {
            fade.color = new Vector4(1f, 1f, 1f, 1f);
            yield break;
        }
        while (timer <= time)
        {
            var a_phase = Mathf.Lerp(0f, 1f, timer / time);
            fade.color = new Vector4(1f, 1f, 1f, a_phase);
            timer += Time.deltaTime;
            if (CheckSkip())
            {
                timer = time;
            }
            yield return null;
        }
    }
    private IEnumerator FadeIn(float _color, float time)
    {        
        var timer = 0f;
        if (time == 0)
        {
            fade.color = new Vector4(_color, _color, _color, 0f);
            yield break;
        }
        while(timer <= time)
        {
            var a_phase = Mathf.Lerp(1f , 0f, timer/time);
            fade.color = new Vector4(_color, _color, _color, a_phase);
            timer += Time.deltaTime;
            if(CheckSkip())
            {
                timer = time;                
            }
            yield return null;
        }        
    }
    private IEnumerator FadeOut(float _color, float time)
    {
        yield return new WaitForEndOfFrame();
        var timer = 0f;
        if(time == 0)
        {
            fade.color = new Vector4(_color, _color, _color, 1f);
            yield break;
        }
        while (timer <= time)
        {
            var a_phase = Mathf.Lerp(0f, 1f, timer / time);
            fade.color = new Vector4(_color, _color, _color, a_phase);
            timer += Time.deltaTime;
            if(CheckSkip())
            {
                timer = time;
            }
            yield return null;
        }        
    }
    #endregion

    [System.Serializable]
    public struct CutsceneElement
    {
        public string elememtName;
        public float transitoinTime;
        public float transiontDelay;
        public Transition_In t_In;
        public Transition_Out t_Out;
        public Sprite backgroudImage;
        public DialogBoxRelay dialogWindow;
        [TextArea] public string text;
        public UnityEngine.Events.UnityEvent[] preDialogAction;
        public UnityEngine.Events.UnityEvent[] atTransitionAction;
        public UnityEngine.Events.UnityEvent[] postDialogAction;
    }
}
