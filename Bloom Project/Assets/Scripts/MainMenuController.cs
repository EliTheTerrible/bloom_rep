using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenuController : MonoBehaviour
{
    public string cutsceneSceneName;
    public float blackoutTime;
    public Image blackscreen;
    public Slider master, sfx, music;
    public GameObject settingsScreen;
    public VolumeManager vm;
  
    public void StartGame()
    {
        StartCoroutine(StartGameCR());
    }

    [ContextMenu("Set Default Volume")]
    public void SetDefaultVolume()
    {
        VolumeManager.master = master.value;
        VolumeManager.sfxCurrent = sfx.value;
        VolumeManager.musicCurrent = master.value;
    }

    public void OnEnable()
    {
        Screen.SetResolution(1920, 1080, FullScreenMode.FullScreenWindow);
        StartCoroutine(FadeIn());
        master.value = VolumeManager.master;
        sfx.value = VolumeManager.sfxCurrent;
        music.value = VolumeManager.musicCurrent;
        vm.UpdateMasterVolume();
        Time.timeScale = 1;
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && settingsScreen.activeSelf)
        {
            settingsScreen.SetActive(false);
        }
    }

    IEnumerator FadeIn()
    {
        blackscreen.color = new Vector4(0, 0, 0, 0);
        var t = 0f;
        while (t <= blackoutTime)
        {
            var _phase = t / blackoutTime;
            blackscreen.color = new Vector4(0, 0, 0, 1 - _phase);
            t += Time.deltaTime;
            if (!blackscreen.gameObject.activeSelf)
                blackscreen.gameObject.SetActive(true);
            yield return null;
        }
    }

    IEnumerator StartGameCR()
    {
        var t = 0f;
        while (t <= blackoutTime)
        {
            var _phase = t / blackoutTime;
            blackscreen.color = new Vector4(0, 0, 0, _phase);
            t += Time.deltaTime;
            if (!blackscreen.gameObject.activeSelf)
                blackscreen.gameObject.SetActive(true);
            yield return null;
        }
        SceneManager.LoadScene(cutsceneSceneName);
        yield return null;
    }

    IEnumerator ExitCR()
    {
        var t = 0f;
        while (t <= blackoutTime)
        {
            var _phase = t / blackoutTime;
            blackscreen.color = new Vector4(0, 0, 0, _phase);
            t += Time.deltaTime;
            if (!blackscreen.gameObject.activeSelf)
                blackscreen.gameObject.SetActive(true);
            yield return null;
        }
        Application.Quit();
    }

    public void SettingOn()
    {
        settingsScreen.SetActive(true);
        Debug.Log("Check");
    }

    public void SettingOff()
    {
        settingsScreen.SetActive(true);
        Debug.Log("Check");
    }

    public void Exit()
    {
        StartCoroutine(ExitCR());
    }
}
