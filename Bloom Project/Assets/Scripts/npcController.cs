﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class npcController : MonoBehaviour
{
    public string _npcName;    
    public ZoneTrigger dialogTrigger;
    public DialogController dialogController;    
    public Animator anim;

    public bool Overriden = false;
    public bool roam;
    public Collider[] mainColliders;
    public npcMover mover;
    public float roamSpeed;    
    public Transform roamCenter;


    private void OnEnable()
    {
        if (dialogTrigger == null)
            dialogTrigger = GetComponentInChildren<ZoneTrigger>();
        if (!TryGetComponent<Animator>(out anim))
            Debug.LogWarning("No animator on " + _npcName);                
    }


    public void Update()
    {
        if(!Overriden)
        {
            if(dialogTrigger != null)
            {
                dialogController.SetIndicator(dialogTrigger.InRange());
                if (dialogTrigger.InRange())
                {
                    if (roam)
                    {
                        roam = false;
                    }
                    CheckDistace();
                }
                else
                {
                    if (PlayerControler.current.nearestNPC == this)
                    {
                        PlayerControler.current.nearestNPC = null;
                    }
                    if (mover != null)
                    {
                        roam = mover.freeze;
                    }
                    else
                    {
                        roam = false;
                    }
                }
            }             
        }
        else
        {
            roam = false;
            dialogController.SetIndicator(false);
        }
    }
    private void CheckDistace()
    {
        if (PlayerControler.current.nearestNPC != this)
        {
            if (PlayerControler.current.InDialog())
                return;
            if (PlayerControler.current.nearestNPC == null)
            {
                PlayerControler.current.nearestNPC = dialogController.npc;
                return;
            }
            
            var _thisDistanceToPlayer = Vector2.Distance(transform.position, PlayerControler.current.transform.position);
            var _otherDistanceToPlayer = Vector2.Distance(PlayerControler.current.nearestNPC.transform.position, PlayerControler.current.transform.position);

            if (_thisDistanceToPlayer > _otherDistanceToPlayer)
            {
                PlayerControler.current.nearestNPC = dialogController.npc;
            }            
        }
    }
}
