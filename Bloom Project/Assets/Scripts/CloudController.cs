using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudController : MonoBehaviour
{
    public float scrollSpeed;
    public MeshRenderer mesh;

    public void Update()
    {
        if(mesh.material.mainTextureOffset.x >= 1)
        {
            mesh.material.mainTextureOffset = new Vector2(mesh.material.mainTextureOffset.x - 1, mesh.material.mainTextureOffset.y);
        }
        mesh.material.mainTextureOffset = new Vector2(mesh.material.mainTextureOffset.x + scrollSpeed/100f * Time.deltaTime, mesh.material.mainTextureOffset.y);
    }
}
