﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Dialog Option")]
public class DialogOption : Dialog
{
    public Sprite expresion;
    [TextArea] public string text;        
    public UnityEvent[] actions;
    public List<Dialog> nextOption = new List<Dialog>();
    public string[] branchText;    
}

