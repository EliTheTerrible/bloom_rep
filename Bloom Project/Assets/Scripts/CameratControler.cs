using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameratControler : MonoBehaviour
{
    public ZoneTrigger verticalTrack;
    public ZoneTrigger horizontalTrack;
    public GameObject exterior;
    bool outside;
    Transform _transform;


    public void Update()
    {
        if(_transform == null)
        {
            _transform = transform;
        }
        outside = exterior.activeSelf ? true : false;
        if(outside)
        {            
            if(_transform.parent != null)
            {
                _transform.parent = null;
            }
            var currentPostion = _transform.position;
            var track_x = horizontalTrack.InRange() ? PlayerControler.current.transform.position.x : currentPostion.x;
            var track_y = verticalTrack.InRange() ? PlayerControler.current.transform.position.y : currentPostion.y;
            _transform.position = new Vector3(track_x, track_y, currentPostion.z);            
        }
        else
        {
            _transform.parent = PlayerControler.current.transform;
            _transform.localPosition = new Vector3(0f, 0f, -1f);
        }
    }
}
