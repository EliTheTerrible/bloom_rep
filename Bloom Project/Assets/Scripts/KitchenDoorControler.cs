﻿using UnityEngine;
using System.Collections;

public class KitchenDoorControler : MonoBehaviour
{
    public static KitchenDoorControler current;
    public GameObject openDoor;
    public GameObject ClosedDoor;

    public void OnEnable()
    {
        if(current != this)
        {
            if(current != null)
            {
                current.enabled = false;
            }
            current = this;
        }
    }

    public void OpenDoor()
    {
        openDoor.SetActive(true);
        ClosedDoor.SetActive(false);
    }

    public void CloseDoor()
    {
        openDoor.SetActive(false);
        ClosedDoor.SetActive(true);
    }
}
