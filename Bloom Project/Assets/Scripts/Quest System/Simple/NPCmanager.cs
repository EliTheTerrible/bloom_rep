﻿using UnityEngine;
using System.Collections;

public class NPCmanager : MonoBehaviour
{

    public static NPCmanager current;

    public void Awake()
    {
        if(current != this)
        {
            if(current != null)
            {
                current.enabled = false;
            }
            current = this;
        }
    }
}
