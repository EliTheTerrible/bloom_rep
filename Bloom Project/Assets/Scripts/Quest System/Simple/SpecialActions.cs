﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu(menuName = "Special Action Relay")]
public class SpecialActions : ScriptableObject
{
    public enum Seed { air, earht, water, fire }
    public static void GetSeeds()
    {
        SpecialActionControler.current.StartSF_GetSeed();
    }
    
    public static void HealTrees()
    {
        SpecialActionControler.current.TriggerTreesHealing();
    }
    public static void TeleportSFtoPos_1()
    {
        var _sunflower = SpecialActionControler.current.sunflower;
        var _pos_1 = SpecialActionControler.current.sf_home.position;
        SpecialActionControler.current.TeleportNPCToPos(_sunflower, _pos_1, false);        
    }
    public static void TeleportSFtoTree()
    {
        var _sunflower = SpecialActionControler.current.sunflower;
        var _aTree = SpecialActionControler.current.at_pos.position;
        SpecialActionControler.current.TeleportNPCToPos(_sunflower, _aTree, true);
        SpecialActionControler.current.sunflower.mover.roamWP = new System.Collections.Generic.List<Transform>();
        SpecialActionControler.current.sunflower.mover.roamWP.Add(SpecialActionControler.current.at_pos);
    }

    public static void Pickup_watercan()
    {
        var watercan = SpecialActionControler.current.watercan_object;
        SpecialActionControler.current.watercan_object.dialogController.EndDialog();
        SpecialActionControler.current.AddItem(SpecialActionControler.current.watercan_item);                
        SpecialActionControler.current.watercan_object.gameObject.SetActive(false);
        PlayerControler.current.nearestNPC = null;
    }
    
    public static void Add_Seeds()
    {
        SpecialActionControler.current.AddItem(SpecialActionControler.current.air_gem_seed);
        SpecialActionControler.current.AddItem(SpecialActionControler.current.earth_gem_seed);
        SpecialActionControler.current.AddItem(SpecialActionControler.current.fire_gem_seed);
        SpecialActionControler.current.AddItem(SpecialActionControler.current.water_gem_seed);
    }
    public static void SunflowerDialogInjection(Dialog _dialogToInject)
    {
        if(SpecialActionControler.current.sunflower.dialogController.standbyOption != _dialogToInject)
        {
            SpecialActionControler.current.sunflower.dialogController.standbyOption = SpecialActionControler.current.sunflower.dialogController.defaultOption;
        }
        SpecialActionControler.current.sunflower.dialogController.currentOption = _dialogToInject;
    }
    public static void AncientTreeDialogInjection(Dialog _dialogToInject)
    {
        if (SpecialActionControler.current.ancientTree.dialogController.standbyOption != _dialogToInject)
        {
            SpecialActionControler.current.ancientTree.dialogController.standbyOption = SpecialActionControler.current.sunflower.dialogController.currentOption;
        }
        SpecialActionControler.current.ancientTree.dialogController.currentOption = _dialogToInject;
    }
    public void SunflowerToDeafaultDialog()
    {
        if(SpecialActionControler.current.sunflower.dialogController.standbyOption != null)        
            SpecialActionControler.current.sunflower.dialogController.currentOption = SpecialActionControler.current.sunflower.dialogController.standbyOption;        
        else
            SpecialActionControler.current.sunflower.dialogController.currentOption = SpecialActionControler.current.sunflower.dialogController.defaultOption;        

    }    

    public void PMdialogToDefault()
    {
        SpecialActionControler.current.playerMailbox.dialogController.currentOption = SpecialActionControler.current.playerMailbox.dialogController.defaultOption;
    }
    public static void PlantSeed(int _seed)
    {        
        if(_seed == 0)
        {
            SpecialActionControler.current.Plant(SpecialActionControler.current.air_gem_seed);
        }
        if (_seed == 1)
        {
            SpecialActionControler.current.Plant(SpecialActionControler.current.earth_gem_seed);

        }
        if (_seed == 2)
        {
            SpecialActionControler.current.Plant(SpecialActionControler.current.fire_gem_seed);

        }
        if (_seed == 3)
        {            
            SpecialActionControler.current.Plant(SpecialActionControler.current.water_gem_seed);
        }
    }
    
    public static void GiveCrystal()
    {
        InventoryManager.current.RemoveSelectedItem();
    }
    public static void HarvestCrystal(int _crystal)
    {
        if (_crystal == 0)
        {
            SpecialActionControler.current.Harvest(SpecialActionControler.current.air_gem_seed);
        }
        if (_crystal == 1)
        {
            SpecialActionControler.current.Harvest(SpecialActionControler.current.earth_gem_seed);

        }
        if (_crystal == 2)
        {
            SpecialActionControler.current.Harvest(SpecialActionControler.current.fire_gem_seed);

        }
        if (_crystal == 3)
        {
            SpecialActionControler.current.Harvest(SpecialActionControler.current.water_gem_seed);
        }
    }
    public void CloseAndSkip(Dialog _nextDialog)
    {
        PlayerControler.current.currentDialog.currentOption = _nextDialog;
        PlayerControler.current.currentDialog.EndDialog();
    }    
    public static void AdvanceQuestStep()
    {
        DemoQuest.current.AdvanceStep();
    }
    public static void RefillCan()
    {
        WaterManager.current.Refill();
    }
    public static void Water(int _crystal)
    {
        if(_crystal == 0)
        {
            SpecialActionControler.current.StartWatering(SpecialActionControler.current.air_gem_seed);
        }
        if (_crystal == 1)
        {
            SpecialActionControler.current.StartWatering(SpecialActionControler.current.earth_gem_seed);
        }
        if (_crystal == 2)
        {
            SpecialActionControler.current.StartWatering(SpecialActionControler.current.fire_gem_seed);
        }
        if (_crystal == 3)
        {
            SpecialActionControler.current.StartWatering(SpecialActionControler.current.water_gem_seed);
        }
    }
    public static void AddWood()
    {
        InventoryManager.current.AddItem(SpecialActionControler.current.wood);
    }
    public static void PlaySFX(AudioClip _sfx)
    {
        SpecialActionControler.current.playerSfx.PlayOneShot(_sfx);
    }
    public static void RideIntoTheSunset()
    {
        SpecialActionControler.current.WalkToBridge();
    }

    public void SetPlayerSpeed(float speed)
    {
        SpecialActionControler.current.UpdatePlayerSpeed(speed);
    }
}
