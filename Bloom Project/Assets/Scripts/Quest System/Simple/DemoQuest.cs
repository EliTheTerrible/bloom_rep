﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class DemoQuest : MonoBehaviour
{
    public static DemoQuest current;
    
    public List<Quest_Step> steps = new List<Quest_Step>();
    public List<Dialog> seenDialog;
    public int currentStep = 0;

    private void Awake()
    {
        if (current != this)
        {
            if(current != null)
            {
                current.enabled = false;
            }
            current = this;
        }
    }
    private void Start()
    {
        currentStep = 0;
        foreach (Quest_Step _nextStep in steps)
        {
            foreach (QuestNPCInfo _npcToUpdate in steps[currentStep].participatingNpcs)
            {
                _npcToUpdate.npc.dialogController.defaultOption = _npcToUpdate.questDefaultText;
            }
        }
    }

    public bool CheckStep()
    {
        var _stepComplete = true;       
        foreach (DialogOption _dialogToCheck in steps[currentStep].requiredInteractions)
        {
            if(!seenDialog.Contains(_dialogToCheck))
            {
                _stepComplete = false;
            }
        }        
        return  _stepComplete;
    }

    public void AddSeenDialog(Dialog optionToAdd)
    {
        if (!seenDialog.Contains(optionToAdd))
            seenDialog.Add(optionToAdd);
        if(CheckStep())
            AdvanceStep();  
    }

    public void AdvanceStep()
    {
        if(currentStep + 1 == steps.Count)
        {
            // on last step
            return;
        }
        currentStep += 1;
        foreach(QuestNPCInfo _npcToUpdate in steps[currentStep].participatingNpcs)
        {
            _npcToUpdate.npc.dialogController.defaultOption = _npcToUpdate.questDefaultText;
            _npcToUpdate.npc.dialogController.currentOption = _npcToUpdate.questDefaultText;
        }
        foreach(UnityEvent _globalAction in steps[currentStep].globalActions)
        {
            _globalAction.Invoke();
        }
        seenDialog = new List<Dialog>();
    }

    [System.Serializable]
    public struct Quest_Step
    {        
        public DialogOption[] requiredInteractions;[Space]        
        public QuestNPCInfo[] participatingNpcs;        
        public UnityEvent[] globalActions;
    }

    [System.Serializable]
    public struct QuestNPCInfo
    {
        public npcController npc;
        public Dialog  questDefaultText;

    }
}
