using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager current;
    public Transform storage;
    public List<Image> slots = new List<Image>();
    public Dictionary<Image, Item> inventoryState = new Dictionary<Image, Item>();
    public Item selectedItem;
    
    public void Awake()
    {
        if(current != this)
        {
            Initialize();
        }
    }
    private void Initialize()
    {
        if (current != null)
        {
            current.enabled = false;
        }
        current = this;
        if (!PlayerControler.current.inventory.activeSelf)
            PlayerControler.current.inventory.SetActive(true);
        foreach (Image slotToInitialize in slots)
        {
            inventoryState.Add(slotToInitialize, null);
        }
        UpdateSprites();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            SelectSlot(0);
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            SelectSlot(1);
        }
        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            SelectSlot(2);
        }
        if(Input.GetKeyDown(KeyCode.Alpha4))
        {
            SelectSlot(3);
        }
        if(Input.GetKeyDown(KeyCode.Alpha5))
        {
            SelectSlot(4);
        }
    }
    public void SelectSlot(int _slot)
    {
        Deselect();
        if(inventoryState[slots[_slot]] == null)
        {
            selectedItem = null;
        }
        else
        {            
            selectedItem = inventoryState[slots[_slot]];
            slots[_slot].transform.localScale = Vector3.one * 1.3f;
        }
    }
    public void Deselect()
    {
        selectedItem = null;
        foreach(Image _slotToDeselect in slots)
        {
            _slotToDeselect.transform.localScale = Vector3.one;
        }
    }
    public void AddItem(Item _ItemToAdd)
    {
        if(!CheckForSpace())
        {
            Debug.Log("Inventory Full!");
            return;
        }

        Image _slotToUse = null;
        for(int i = 0; i < slots.Count; ++i)
        {
            if (_slotToUse != null)
                break;
            if (inventoryState[slots[i]] == null)
            {
                _slotToUse = slots[i];                
            }
        }
        if(_slotToUse == null)
        {
            Debug.Log("Couldn't find empty slot!");
            return;
        }

        inventoryState[_slotToUse] = _ItemToAdd;
        UpdateSprites();
        _ItemToAdd.transform.parent = storage;
    }
    public void RemoveSelectedItem()
    {        
        for(int i = 0; i < slots.Count; ++i)
        {
            if (inventoryState[slots[i]] == selectedItem)
            {
                inventoryState[slots[i]] = null;
            }
        }
        selectedItem = null;
        UpdateSprites();
    }
    public void UpdateSprites()
    {        
        foreach(Image _slotToUpdate in inventoryState.Keys)
        {
            if (inventoryState[_slotToUpdate] != null)
            {
                _slotToUpdate.enabled = true;
                _slotToUpdate.sprite = inventoryState[_slotToUpdate].displaySprite;
            }
            else
            {
                _slotToUpdate.sprite = null;
                _slotToUpdate.enabled = false;
            }
        }
    }
    private bool CheckForSpace()
    {        
        foreach (Image _slotToCheck in inventoryState.Keys)
        {
            if (inventoryState[_slotToCheck] == null)
            {
                return true;
            }
        }
        return false;
    }
}
