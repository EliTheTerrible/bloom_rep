using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Injected Dialog")]
public class DialogOptionInjection : ScriptableObject
{
    public string npcToInjectName;
    public Dialog dialogToInject;
}
