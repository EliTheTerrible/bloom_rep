﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
public class ZoneTrigger : MonoBehaviour
{
    bool _playerInZone;
    public UnityEvent onEnterEvent;
    public UnityEvent onExitEvent;


    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            _playerInZone = true;
            if(onEnterEvent != null)
            {
                onEnterEvent.Invoke();
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            _playerInZone = false;
            if (onExitEvent != null)
            {
                onExitEvent.Invoke();
            }
        }                
    }

    public bool InRange()
    {
        return _playerInZone;
    }
    
    public void EnableTransition(Area area)
    {
        AreaSwitcher.current.EnableTransition(area);
    }

    public void Autotransiotion(Area area)
    {
        AreaSwitcher.current.AutoTransition(area);
    }

    public void DisableTransition()
    {
        AreaSwitcher.current.DisableTransition();
    }    
}
